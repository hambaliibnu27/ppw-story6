from django.shortcuts import render, redirect
from .models import Member, Activity
from .forms import MemberForm, ActivityForm
from django.contrib import messages


# Create your views here.

def activity(request):
    if request.method == "POST":
        formA = ActivityForm(request.POST)
        formB = MemberForm(request.POST)
        if formA.is_valid():
            formA.save()
            messages.success(request, (f"Kegiatan {request.POST['activity']} telah ditambahkan!"))
            return redirect('home:activity')
        elif formB.is_valid():
            formB.save()
            messages.success(request, (f"{request.POST['member']} telah bergabung!"))
            return redirect('home:activity')
        else:
            messages.warning(request, (f"Input tidak sesuai!"))
            return redirect('home:activity')
    else:
        formA = ActivityForm()
        formB = MemberForm()
        activities = Activity.objects.all()
        members = Member.objects.all()
        context = {
            'formA' : formA,
            'formB' : formB,
            'activities' : activities,
            'members' : members
        }
        return render(request, 'home/index.html', context)

def delete(request,pk):
    activity = Activity.objects.get(id=pk)
    activity.delete()
    messages.warning(request, (f"Kegiatan {activity} telah dihapus!"))
    return redirect('home:activity')

def deleteMember(request,pk):
    member = Member.objects.get(id=pk)
    member.delete()
    messages.warning(request, (f"{member} telah dihapus!"))
    return redirect('home:activity')
