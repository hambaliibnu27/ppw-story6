from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Activity, Member



class TestRandom(TestCase):
    def setUp(self):
        self.client = Client()
        self.activity_url = reverse("home:activity")
        self.test_activity = Activity.objects.create(
            activity = "minum"
        )
        self.test_member = Member.objects.create(
            member = "ibnu",
            aktivitas = self.test_activity
        )
        self.test_delete_url = reverse("home:activityDelete",args=[self.test_activity.id])
        self.test_member_delete_url = reverse("home:memberDelete",args=[self.test_member.id])

    def test_cek_aktivitas_GET(self):
        response = self.client.get(self.activity_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "home/index.html")

    def test_cek_aktivitas_POST(self):
        response = self.client.post(self.activity_url, {
            "activity" : "begadang"
        }, follow=True)
        self.assertContains(response, "begadang")

    def test_cek_member_POST(self):
        response = self.client.post(self.activity_url, {
            "member" : "okto",
            "aktivitas" : self.test_activity.id,
        }, follow=True)
        self.assertContains(response, "okto")

    def test_cek_validasi_POST(self):
        response = self.client.post(self.activity_url, {
            "member" : "okto",
            "aktivitas" : "haha",
        }, follow=True)
        self.assertContains(response, "Input tidak sesuai")

    def test_cek_member_DELETE(self):
        response = self.client.get(self.test_member_delete_url, follow=True)
        print(response.content)
        self.assertContains(response, "telah dihapus")

    def test_cek_aktivitas_DELETE(self):
        response = self.client.get(self.test_delete_url, follow=True)
        self.assertContains(response, "telah dihapus")

